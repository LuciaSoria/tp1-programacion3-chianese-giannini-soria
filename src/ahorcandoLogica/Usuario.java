package ahorcandoLogica;

public class Usuario implements Comparable<Usuario> {
	
	private String _nombre;
	private Integer _puntos;

	public Usuario(String nombre, Integer puntos) {
		_nombre = nombre;
		_puntos = puntos;
	}

	public String getNombre() {
		return _nombre;
	}

	public Integer getPuntos() {
		return _puntos;
	}
	
	@Override
	public int compareTo(Usuario otroUsuario) {
		return (int) _puntos - (int) otroUsuario.getPuntos();
	}
}